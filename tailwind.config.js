module.exports = {
  content: ['./index.html'],
  theme: {
    container:{
      center: true,
    },
    fontFamily: {
      'manrope': ['Manrope'],
    },
    extend: {
      screens: {
        '2xl': '1120px',
        'xl': '800px',
        'md': '768px',
      }
    },
  },
  plugins: [],
}
