//Services Div
function dropDown1() {
    var x = document.getElementById("awareness");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById("performance").style.display = "none";
        document.getElementById("dropdown-1").style.color = "white";
        document.getElementById("dropdown-2").style.color = "black";
        document.getElementById("dropdown-3").style.color = "black";
        document.getElementById("content-marketing").style.display = "none";
    } else {
        x.style.display = "none";
        document.getElementById("dropdown-2").style.color = "white";
        document.getElementById("dropdown-3").style.color = "white";
    }
}

function dropDown2() {
    var x = document.getElementById("performance");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById("awareness").style.display = "none";
        document.getElementById("dropdown-1").style.color = "black";
        document.getElementById("dropdown-2").style.color = "white";
        document.getElementById("dropdown-3").style.color = "black";
        document.getElementById("content-marketing").style.display = "none";
    } else {
        x.style.display = "none";
        document.getElementById("dropdown-1").style.color = "white";
        document.getElementById("dropdown-3").style.color = "white";
    }
}

function dropDown3() {
    var x = document.getElementById("content-marketing");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById("awareness").style.display = "none";
        document.getElementById("dropdown-1").style.color = "black";
        document.getElementById("dropdown-2").style.color = "black";
        document.getElementById("dropdown-3").style.color = "white";
        document.getElementById("performance").style.display = "none";
    } else {
        x.style.display = "none";
        document.getElementById("dropdown-1").style.color = "white";
        document.getElementById("dropdown-2").style.color = "white";
    }
}

//ParticlesJS

particlesJS("particles-js", {
    particles: {
        number: { value: 115, density: { enable: true, value_area: 800 } },
        color: { value: "#8c8c8c" },
        shape: { type: "circle", stroke: { width: 2, color: "#dbdbdb" }, polygon: { nb_sides: 4 }, image: { src: "img/github.svg", width: 100, height: 100 } },
        opacity: { value: 0.352750653390415, random: false, anim: { enable: false, speed: 1, opacity_min: 0.1, sync: false } },
        size: { value: 7, random: true, anim: { enable: false, speed: 12.181158184520175, size_min: 1.6241544246026904, sync: true } },
        line_linked: { enable: true, distance: 176.3753266952075, color: "#dbdbdb", opacity: 0.4, width: 1.4430708547789706 },
        move: { enable: true, speed: 2, direction: "none", random: false, straight: false, out_mode: "out", bounce: false, attract: { enable: false, rotateX: 600, rotateY: 1200 } },
    },
    interactivity: {
        detect_on: "canvas",
        events: { onhover: { enable: false, mode: "repulse" }, onclick: { enable: true, mode: "push" }, resize: true },
        modes: {
            grab: { distance: 400, line_linked: { opacity: 1 } },
            bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
            repulse: { distance: 200, duration: 0.4 },
            push: { particles_nb: 4 },
            remove: { particles_nb: 2 },
        },
    },
    retina_detect: true,
});
var count_particles, stats, update;
stats = new Stats();
stats.setMode(0);
stats.domElement.style.position = "absolute";
stats.domElement.style.left = "0px";
stats.domElement.style.top = "0px";
document.body.appendChild(stats.domElement);
count_particles = document.querySelector(".js-count-particles");
update = function () {
    stats.begin();
    stats.end();
    if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
        count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
    }
    requestAnimationFrame(update);
};
requestAnimationFrame(update);

// ParticlesJS-2

particlesJS("particles-js-2", {
    particles: {
        number: { value: 115, density: { enable: true, value_area: 800 } },
        color: { value: "#8c8c8c" },
        shape: { type: "circle", stroke: { width: 2, color: "#dbdbdb" }, polygon: { nb_sides: 4 }, image: { src: "img/github.svg", width: 100, height: 100 } },
        opacity: { value: 0.352750653390415, random: false, anim: { enable: false, speed: 1, opacity_min: 0.1, sync: false } },
        size: { value: 7, random: true, anim: { enable: false, speed: 12.181158184520175, size_min: 1.6241544246026904, sync: true } },
        line_linked: { enable: true, distance: 176.3753266952075, color: "#dbdbdb", opacity: 0.4, width: 1.4430708547789706 },
        move: { enable: true, speed: 2, direction: "none", random: false, straight: false, out_mode: "out", bounce: false, attract: { enable: false, rotateX: 600, rotateY: 1200 } },
    },
    interactivity: {
        detect_on: "canvas",
        events: { onhover: { enable: false, mode: "repulse" }, onclick: { enable: true, mode: "push" }, resize: true },
        modes: {
            grab: { distance: 400, line_linked: { opacity: 1 } },
            bubble: { distance: 400, size: 40, duration: 2, opacity: 8, speed: 3 },
            repulse: { distance: 200, duration: 0.4 },
            push: { particles_nb: 4 },
            remove: { particles_nb: 2 },
        },
    },
    retina_detect: true,
});
var count_particles, stats, update;
stats = new Stats();
stats.setMode(0);
stats.domElement.style.position = "absolute";
stats.domElement.style.left = "0px";
stats.domElement.style.top = "0px";
document.body.appendChild(stats.domElement);
count_particles = document.querySelector(".js-count-particles");
update = function () {
    stats.begin();
    stats.end();
    if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
        count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
    }
    requestAnimationFrame(update);
};


// Navbar fixed

window.onscroll = function() {
    const header = document.querySelector('header');
    const fixedNav = header.offsetTop;

    if(window.pageYOffset > fixedNav) {
        header.classList.add('navbar-fixed');
    } else {
        header.classList.remove('navbar-fixed');
    }
};

// Hamburger

const hamburger = document.querySelector('#hamburger');
const navMenu = document.querySelector('#nav-menu');

hamburger.onclick = function() {navBar()}

function navBar(){
    if(hamburger.classList.contains('hamburger-active')){
        hamburger.classList.remove('hamburger-active');
        navMenu.classList.add('hidden');        
    } else{
        hamburger.classList.add('hamburger-active');
        navMenu.classList.remove('hidden');
    }
};


const menuMobile = document.getElementsByClassName('menu-mobile');

for(var i = 0; i < menuMobile.length; i++){
    menuMobile[i].onclick = function () {
        hamburger.classList.remove('hamburger-active');
        navMenu.classList.add('hidden');
    };
};